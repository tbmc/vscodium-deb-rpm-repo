# vscodium-deb-rpm-repo

Always up-to-date [VSCodium](https://github.com/VSCodium/vscodium) repository

[![Daily Update Status](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/badges/master/pipeline.svg)](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/commits/master)


## How to install for RPM-based Linux distributions

### Add the repository:

- **Fedora/RHEL**:
```bash
sudo tee -a /etc/yum.repos.d/vscodium.repo << 'EOF'
[gitlab.com_paulcarroty_vscodium_repo]
name=gitlab.com_paulcarroty_vscodium_repo
baseurl=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg
EOF
```

- **openSUSE/SUSE**:
```bash
sudo tee -a /etc/zypp/repos.d/vscodium.repo << 'EOF'
[gitlab.com_paulcarroty_vscodium_repo]
name=gitlab.com_paulcarroty_vscodium_repo
baseurl=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg
EOF
```
### Then type `codium` in [GNOME Software](https://wiki.gnome.org/Apps/Software) or use your package manager:

- `dnf install codium`
- `zypper in codium`



## How to install for **Debian/Ubuntu/Linux Mint**

### Option 1. Add Repo With software-properties-common (i.e. apt-key)

If **software-properties-common** is available, you can use `apt-add` and `apt-add-repository` to add the new repository and its key.

```bash
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -
sudo apt-add-repository 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main'
sudo apt update
sudo apt install codium
```

### Option 2. Add Repo "Manually"

Add my key:
- `wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/vscodum-archive-keyring.gpg`
 
Add the repository:
- `echo 'deb [signed-by=/usr/share/keyrings/vscodum-archive-keyring.gpg] https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list`

Update then install vscodium:
- `sudo apt update`
- `sudo apt install codium`

Then search for `codium` and run it (e.g. the Activities menu from the Gnome Panel, or whatever else you use as your launcher or application manager).


## Verification

Checksum verification doesn't works 'cause GPG signature change the size of package.
You can use `diff -r` for extracted packages or [pkgdiff](https://github.com/lvc/pkgdiff).

## Updates?

[Every](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/commits/repos) 24 hours at 03:00 UTC.


## Packages for another Linux distributions

Just make a pull request.
